export default {
  AUTH_TOKEN: 'bWFydGlhbmFuZG1hY2hpbmU=',
  REQUESTS: {
    USERS: '/api/users',
    POSTS: '/api/posts',
    COMMENTS: '/api/comments',
  },
};
