import { getPosts, getUsers, getComments } from '../__mocks__/requests';

describe('#getPosts() using Promises', () => {
  it('checks all posts & posts properties', async () => {
    expect.hasAssertions();
    const response = await getPosts();
    const data = response.data;
    expect(response).not.toBeNull();
    expect(data).not.toBeNull();
    expect(data.length).toEqual(3);

    checkAllItems(data, postPropertyTypesChecker);
  });
});

describe('#getUsers() using Promises', () => {
  it('checks all users & user properties', async () => {
    expect.hasAssertions();
    const response = await getUsers();
    const data = response.data;
    expect(response).not.toBeNull();
    expect(data).not.toBeNull();
    expect(data.length).toEqual(3);

    checkAllItems(data, userPropertyTypesChecker);
  });
});

describe('#getComments() using Promises', () => {
  it('checks all comments & comment properties', async () => {
    expect.hasAssertions();
    const response = await getComments();
    const data = response.data;
    expect(response).not.toBeNull();
    expect(data).not.toBeNull();
    expect(data.length).toEqual(4);

    checkAllItems(data, commentPropertyTypesChecker);
  });
});

function checkAllItems (data, propertyTypesChecker) {
  for (let i = 0; i < data.length; i++) {
    let item = data[ i ];
    propertyTypesChecker(item);
  }
}

function postPropertyTypesChecker (post) {
  const { id, userId, title, body } = post;
  expect(post).not.toBeNull();
  expect(typeof post).toBe('object');
  expect(typeof id).toBe('number');
  expect(typeof title).toBe('string');
  expect(typeof body).toBe('string');
  expect(typeof userId).toBe('number');
}

function userPropertyTypesChecker (user) {
  const { id, name, email } = user;
  expect(typeof user).toBe('object');
  expect(typeof id).toBe('number');
  expect(typeof name).toBe('string');
  expect(typeof email).toBe('string');
}

function commentPropertyTypesChecker (comment) {
  const { id, postId, name, body, email } = comment;
  expect(comment).not.toBeNull();
  expect(typeof comment).toBe('object');
  expect(typeof id).toBe('number');
  expect(typeof name).toBe('string');
  expect(typeof body).toBe('string');
  expect(typeof postId).toBe('number');
  expect(typeof email).toBe('string');
}
