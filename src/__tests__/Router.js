import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { shallow, mount } from 'enzyme';
import Dictionary from '../dictionary/Dictionary';
import Login from '../containers/Login';
import App from '../containers/App';
import NotFound from '../components/NotFound';

jest.dontMock('../containers/App');

describe('Router will initially show Login Component', () => {
  const wrapper = shallow(
    <MemoryRouter initialEntries={[ '/' ]} initialIndex={0}>
      <Login message={Dictionary.MESSAGE} history={{}} />
    </MemoryRouter>
  );

  it('is on Login Screen', () => {
    expect(wrapper.contains(<Login message={Dictionary.MESSAGE} history={{}} />)).toBe(true);
  });
});

describe('Router will redirect to NotFound', () => {
  it('redirects to NotFound', () => {
    test('invalid path should redirect to NotFound page', () => {
      const wrapper = mount(
        <MemoryRouter initialEntries={[ '/invalid' ]}>
          <App message={Dictionary.MESSAGE} />
        </MemoryRouter>
      );
      expect(wrapper.find(<App message={Dictionary.MESSAGE} />)).toHaveLength(0);
      expect(wrapper.find(<NotFound message={Dictionary.MESSAGE} />)).toHaveLength(1);
    });
  });
});
