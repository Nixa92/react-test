import React from 'react';
import ReactDOM from 'react-dom';
import Main from '../containers/Main';
import Dictionary from '../dictionary/Dictionary';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Main message={Dictionary.MESSAGE} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
