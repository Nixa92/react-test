import React from 'react';
import { shallow, render } from 'enzyme';
import Login from '../containers/Login';
import Dictionary from '../dictionary/Dictionary';

describe('Login Component', () => {
  it('should render without throwing an error', () => {
    expect(shallow(<Login message={Dictionary.MESSAGE} history={{}} />).exists(<form />)).toBe(true);
  });

  it('renders a username input', () => {
    expect(render(<Login message={Dictionary.MESSAGE} history={{}} />).find('#username').length).toEqual(1);
  });

  it('renders a password input', () => {
    expect(render(<Login message={Dictionary.MESSAGE} history={{}} />).find('#password').length).toEqual(1);
  });

  it('renders a login button', () => {
    expect(render(<Login message={Dictionary.MESSAGE} history={{}} />).find('button').length).toEqual(1);
  });
});
