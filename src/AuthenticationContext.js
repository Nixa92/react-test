import React from 'react';

const AuthenticationContext = React.createContext({
  isAuthenticated: false,
  currentUser: '',
  authenticate: () => {},
  logout: () => {},
});

export default AuthenticationContext;
