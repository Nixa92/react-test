import fs from 'fs';

const getPosts = () => request('posts');
const getUsers = () => request('users');
const getComments = () => request('comments');

const request = url =>
  new Promise((resolve, reject) => {
    // Load json data from a file in de subfolder for mock data
    fs.readFile(`./src/__mockData__/${url}.json`, 'utf8', (error, data) => {
      if (error) reject(error);
      // Parse the data as JSON and put in the key entity (just like the request library does)
      resolve({ data: JSON.parse(data) });
    });
  });

export { request, getPosts, getUsers, getComments };
