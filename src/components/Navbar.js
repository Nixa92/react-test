import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Breadcrumbs from '../containers/Breadcrumbs';
import LogoutArea from './LogoutArea';

export default class Navbar extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { message } = this.props;
    return (
      <nav id='#navbar' className='navbar fixed-top navbar-expand-lg navbar-light bg-light'>
        <a className='navbar-brand'>M&M</a>
        <Breadcrumbs message={message} />
        <LogoutArea message={message} />
      </nav>
    );
  }
}
