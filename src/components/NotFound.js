import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../assets/css/NotFound.css';

export default class NotFound extends Component {
  static propTypes = {
    history: PropTypes.object,
    message: PropTypes.string.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { history } = this.props;

    return (
      <div className='not-found'>
        <h1>404</h1>
        <h2>Seems like... You're lost...</h2>
        <h4>
          {history ? (
            <span>
              Step&nbsp;<a onClick={history.goBack}>back</a>&nbsp;to the last safe location
            </span>
          ) : (
            <span>
              Press this &nbsp;<a className='btn btn-outlined-primary' onClick={() => window.location.reload()}>
                magic button
              </a>&nbsp; or please refresh the page
            </span>
          )}
        </h4>
      </div>
    );
  }
}
