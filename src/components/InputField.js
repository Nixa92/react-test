import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import '../assets/css/NotFound.css';

export default class InputField extends Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    inputGroup: PropTypes.bool,
    icon: PropTypes.object,
    className: PropTypes.string,
    message: PropTypes.string.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { value, onChange, label, id, type, placeholder, inputGroup, icon, className } = this.props;

    return (
      <div className={classNames({ 'form-group': !inputGroup, 'input-group': inputGroup })}>
        {inputGroup && icon}
        <label className='input-label' htmlFor={id}>
          {label}
        </label>
        <input
          placeholder={placeholder}
          id={id}
          type={type}
          className={classNames('form-control', className)}
          value={value}
          onChange={onChange}
          autoComplete='off'
        />
      </div>
    );
  }
}
