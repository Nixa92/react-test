import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../assets/css/SearchField.css';

export default class SearchField extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    render: PropTypes.func.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    return (
      <div className='search-field-wrapper'>
        <div className='search-field'>{this.props.render()}</div>
      </div>
    );
  }
}
