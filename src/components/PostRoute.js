import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from './Loading';
import Toggler from './Toggler';
import Comments from './comments/Comments';
import Post from './Post';
import { Redirect } from 'react-router-dom';

export default class PostRoute extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    location: PropTypes.object,
  };

  state = {
    data: null,
    fetching: true,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);

    this.setState({ ...this.props.location.state, fetching: false });
  }

  render () {
    const { data, fetching } = this.state;
    const { message } = this.props;

    if (fetching) {
      return <Loading message={message} />;
    }

    if (!data) {
      return <Redirect to='/app' />;
    }

    return (
      <div className='app row'>
        <div className='list-group col-lg-6 offset-lg-3 col-md-8 offset-md-2'>
          <Post data={data} message={message} disabled />
          <Toggler data={data} message={message} disabled>
            <Comments />
          </Toggler>
        </div>
      </div>
    );
  }
}
