import React, { Component, Fragment, cloneElement } from 'react';
import PropTypes from 'prop-types';
import Dictionary from '../dictionary/Dictionary';
import classNames from 'classnames';

export default class Toggler extends Component {
  static propTypes = {
    message: PropTypes.string,
    disabled: PropTypes.bool,
    data: PropTypes.object,
    children: PropTypes.object.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  state = { open: false };

  toggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  render () {
    const { disabled, message, children } = this.props;
    const { open } = this.state;
    const data = this.props.data.data;
    const icon = (
      <i
        className={classNames({
          'fa fa-chevron-down': !open,
          'fa fa-chevron-up': open,
        })}
      />
    );

    return (
      <Fragment>
        <small className='list-item-footer toggler'>
          <a onClick={this.toggle} className={classNames({ disabled: disabled || !data.length })}>
            {data.length}&nbsp;&nbsp;{Dictionary.COMMENTS}&nbsp;&nbsp;{disabled || !data.length ? null : icon}
          </a>
        </small>
        {(disabled || open) && cloneElement(children, { data: data, message })}
      </Fragment>
    );
  }
}
