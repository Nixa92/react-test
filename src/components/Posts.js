import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Posts extends Component {
  static propTypes = {
    children: PropTypes.arrayOf(PropTypes.object).isRequired,
    message: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        body: PropTypes.string,
        username: PropTypes.string,
      })
    ).isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { data, children, message } = this.props;
    return (
      <div className='app list-group col-lg-6 offset-lg-3 col-md-8 offset-md-2'>
        {data.map(item => React.Children.map(children, child => React.cloneElement(child, { data: item, message })))}
      </div>
    );
  }
}
