import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AuthenticationContext from '../AuthenticationContext';
import Dictionary from '../dictionary/Dictionary';
import Button from './Button';

export default class LogoutArea extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    return (
      <AuthenticationContext.Consumer>
        {({ currentUser, logout }) => (
          <span className='ml-auto'>
            <span className='navbar-text mr-3'>{currentUser}</span>
            <Button
              type='button'
              text={Dictionary.LOGOUT}
              onClick={logout}
              className='ml-auto btn-outline-primary my-2 my-sm-0'
              message={this.props.message}
            />
          </span>
        )}
      </AuthenticationContext.Consumer>
    );
  }
}
