import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dictionary from '../dictionary/Dictionary';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

export default class Breadcrumb extends Component {
  static propTypes = {
    URL: PropTypes.string.isRequired,
    condition: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { URL, condition } = this.props;
    if (!URL) return null;
    return (
      <li key={`${URL}`} className={classNames('breadcrumb-item', { active: condition })}>
        <Link to={`/${URL}`} className={classNames({ disabled: condition })}>
          {URL === 'app' ? Dictionary.HOME : URL}
        </Link>
      </li>
    );
  }
}
