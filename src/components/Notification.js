import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import '../assets/css/Notifications.css';
import NotificationContext from '../NotificationContext';

export default class Notification extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    return (
      <NotificationContext.Consumer>
        {({ type, content, hideNotification }) =>
          type &&
          content && (
            <ReactCSSTransitionGroup
              transitionName='notification'
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              <div className='notification-wrapper'>
                <div className={`notification alert alert-${type}`}>
                  {content}
                  <button type='button' className='close' onClick={hideNotification}>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </div>
              </div>
            </ReactCSSTransitionGroup>
          )
        }
      </NotificationContext.Consumer>
    );
  }
}
