import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Comment from './Comment';
import '../../assets/css/Comments.css';

export default class Comments extends Component {
  static propTypes = {
    message: PropTypes.string,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
        userName: PropTypes.string.isRequired,
      }).isRequired
    ),
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { data, message } = this.props;

    if (!data.length) return null;

    return (
      <div className={classNames('comments list-group')}>
        {data.map(comment => <Comment key={`${comment.postId}-${comment.id}`} {...comment} message={message} />)}
      </div>
    );
  }
}
