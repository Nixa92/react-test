import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Comment extends Component {
  static propTypes = {
    message: PropTypes.string,
    name: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { name, userName, body } = this.props;

    return (
      <div className='list-group-item comment'>
        <h6 className='mb-1 list-item-header'>{name}</h6>
        <small>{userName}</small>
        <p className='mb-1 list-item-content'>{body}</p>
      </div>
    );
  }
}
