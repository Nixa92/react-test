import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Loading extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    return (
      <div className='app row loading-wrapper'>
        <div className='lds-ripple'>
          <div />
          <div />
        </div>
      </div>
    );
  }
}
