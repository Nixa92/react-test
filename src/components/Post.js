import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

export default class Post extends Component {
  static propTypes = {
    message: PropTypes.string,
    disabled: PropTypes.bool,
    data: PropTypes.shape({
      title: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired,
      userName: PropTypes.string.isRequired,
    }),
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { disabled, data } = this.props;
    const { title, body, userName } = data;

    if (!data) return null;

    return (
      <Fragment>
        <Link
          to={{
            pathname: `app/post/${data.id}`,
            state: { data },
          }}
          className={classNames('list-group-item list-group-item-action flex-column align-items-start', { disabled })}
        >
          <div className='d-flex w-100 justify-content-between'>
            <h5 className='mb-1 list-item-header'>{title}</h5>
          </div>
          <p className='mb-1 list-item-content'>{body}</p>
          <small className='list-item-footer'>
            <span className='username'>{userName}</span>
          </small>
        </Link>
      </Fragment>
    );
  }
}
