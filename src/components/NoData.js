import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class NoData extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { content } = this.props;
    return (
      <div className='app row loading-wrapper'>
        <h3>{content}</h3>
      </div>
    );
  }
}
