import AppConstants from '../config/AppConstants';
import request from './GenericRequest';
import mapPosts from '../mappers/PostsMapper';

export default function stackedRequests () {
  const token = AppConstants.AUTH_TOKEN;
  const requests = [
    request(AppConstants.REQUESTS.USERS, token),
    request(AppConstants.REQUESTS.POSTS, token),
    request(AppConstants.REQUESTS.COMMENTS, token),
  ];

  return Promise.all(requests).then(([ users, posts, comments ]) => mapPosts(users, posts, comments));
}
