import 'whatwg-fetch';

export default function request (URL, token) {
  return fetch(URL, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Auth': token,
    },
  }).then(response => response.json());
}
