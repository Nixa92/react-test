import React from 'react';
import ReactDOM from 'react-dom';
import Main from './containers/Main';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/index.css';
import Dictionary from './dictionary/Dictionary';

ReactDOM.render(<Main message={Dictionary.MESSAGE} />, document.getElementById('root'));
registerServiceWorker();
