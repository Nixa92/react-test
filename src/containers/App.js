import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../assets/css/App.css';
import stackedRequests from '../services/StackedRequests';
import InputField from '../components/InputField';
import Posts from '../components/Posts';
import Post from '../components/Post';
import Toggler from '../components/Toggler';
import Comments from '../components/comments/Comments';
import Loading from '../components/Loading';
import SearchField from '../components/SearchField';
import NoData from '../components/NoData';
import Dictionary from '../dictionary/Dictionary';

export default class App extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  state = {
    data: [],
    filteredData: [],
    filter: '',
    fetching: true,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
    stackedRequests().then(data => this.setState({ data, filteredData: data, fetching: false }));
  }

  filterPosts = e => {
    let filter = e.target.value;
    this.setState(state => {
      const { data } = state;
      filter = filter ? filter.toLocaleLowerCase() : '';
      return {
        filteredData: filter ?
          data.filter(post => post && post.userName && post.userName.toLocaleLowerCase().indexOf(filter) !== -1) :
          data,
        filter: filter,
      };
    });
  };

  render () {
    const { filteredData, filter, fetching } = this.state;
    const { message } = this.props;

    if (fetching) {
      return <Loading message={message} />;
    }

    return (
      <div className='app row'>
        <SearchField
          message={message}
          render={() => (
            <InputField
              value={filter}
              onChange={this.filterPosts}
              label=''
              id='filter'
              type='text'
              inputGroup
              icon={
                <span>
                  <i className='fa fa-search' aria-hidden='true' />
                </span>
              }
              placeholder={Dictionary.FILTER_POSTS_BY_USER}
              message={message}
            />
          )}
        />
        {!filteredData.length && <NoData message={message} content={Dictionary.NO_POSTS} />}
        <Posts data={filteredData} message={message}>
          <Post />
          <Toggler>
            <Comments />
          </Toggler>
        </Posts>
      </div>
    );
  }
}
