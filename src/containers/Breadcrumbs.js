import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Breadcrumb from '../components/Breadcrumb';
import '../assets/css/Breadcrumbs.css';

class Breadcrumbs extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    location: PropTypes.object.isRequired,
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    let breadcrumbs = this.props.location.pathname.split('/');
    let breadcrumbsLength = breadcrumbs.length;

    if (!isNaN(breadcrumbs[ breadcrumbsLength - 1 ])) {
      breadcrumbs.pop();
      breadcrumbsLength--;
    }

    return (
      <ol className='breadcrumb'>
        {breadcrumbs.map((url, index) => (
          <Breadcrumb key={url} URL={url} condition={breadcrumbsLength === index + 1} message={this.props.message} />
        ))}
      </ol>
    );
  }
}

export default withRouter(Breadcrumbs);
