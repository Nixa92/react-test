import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import App from './App';
import PostRoute from '../components/PostRoute';
import NotFound from '../components/NotFound';

class NestedAppRoutes extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    return (
      <Switch>
        <Route exact path='/app' render={props => <App {...this.props} {...props} />} />
        <Route exact path='/app/post/:id' render={props => <PostRoute {...this.props} {...props} />} />
        <Route render={props => <NotFound {...this.props} {...props} />} />
      </Switch>
    );
  }
}

export default NestedAppRoutes;
