import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

const ExtendedRoute = ({ component: Component, condition, message, redirect, ...rest }) => (
  <Route
    {...rest}
    render={props => (condition ? <Component message={message} {...props} /> : <Redirect to={redirect} />)}
  />
);

ExtendedRoute.propTypes = {
  message: PropTypes.string.isRequired,
  component: PropTypes.func.isRequired,
  condition: PropTypes.bool.isRequired,
  redirect: PropTypes.string.isRequired,
};

export default ExtendedRoute;
