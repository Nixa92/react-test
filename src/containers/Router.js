import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { HashRouter, Switch } from 'react-router-dom';
import ExtendedRoute from './HOC/ExtendedRoute';
import Login from './Login';
import Notification from '../components/Notification';
import Navbar from '../components/Navbar';
import NestedAppRoutes from './NestedAppRoutes';
import AuthenticationContext from '../AuthenticationContext';

class Router extends Component {
  static propTypes = { message: PropTypes.string.isRequired };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  render () {
    const { message } = this.props;
    return (
      <AuthenticationContext.Consumer>
        {({ isAuthenticated }) => (
          <Fragment>
            <HashRouter>
              <Fragment>
                {isAuthenticated && <Navbar message={message} />}
                <Switch>
                  <ExtendedRoute
                    exact
                    path='/'
                    condition={!isAuthenticated}
                    component={Login}
                    redirect='/app'
                    message={message}
                  />
                  <ExtendedRoute
                    condition={isAuthenticated}
                    component={NestedAppRoutes}
                    redirect='/'
                    message={message}
                  />
                </Switch>
              </Fragment>
            </HashRouter>
            <Notification message={message} />
          </Fragment>
        )}
      </AuthenticationContext.Consumer>
    );
  }
}

export default Router;
