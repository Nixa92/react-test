import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputField from '../components/InputField';
import '../assets/css/Login.css';
import Button from '../components/Button';
import Dictionary from '../dictionary/Dictionary';
import AuthenticationContext from '../AuthenticationContext';

class Login extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    history: PropTypes.object.isRequired,
  };

  state = {
    username: '',
    password: '',
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  handleInputChange = name => e => {
    this.setState({ [ name ]: e.target.value });
  };

  render () {
    const { username, password } = this.state;
    const { history, message } = this.props;

    return (
      <AuthenticationContext.Consumer>
        {({ authenticate }) => (
          <div className='login'>
            <form onSubmit={() => authenticate(username, password, history)} autoComplete='off'>
              <h3>{Dictionary.LOGIN.TITLE}</h3>
              <InputField
                value={username}
                onChange={this.handleInputChange('username')}
                label={Dictionary.LOGIN.USERNAME}
                id='username'
                type='text'
                placeholder={Dictionary.LOGIN.USERNAME_PLACEHOLDER}
                message={message}
              />
              <InputField
                value={password}
                onChange={this.handleInputChange('password')}
                label={Dictionary.LOGIN.PASSWORD}
                id='password'
                type='password'
                placeholder={Dictionary.LOGIN.PASSWORD_PLACEHOLDER}
                message={message}
              />
              <Button
                text={Dictionary.LOGIN.BUTTON}
                type='submit'
                className='btn-primary btn-block'
                message={message}
              />
            </form>
          </div>
        )}
      </AuthenticationContext.Consumer>
    );
  }
}

export default Login;
