import React from 'react';
import PropTypes from 'prop-types';
import Router from './Router';
import Dictionary from '../dictionary/Dictionary';
import AuthenticationContext from '../AuthenticationContext';
import NotificationContext from '../NotificationContext';

export default class Main extends React.Component {
  static propTypes = { message: PropTypes.string.isRequired };

  state = {
    authentication: {
      isAuthenticated: false,
      currentUser: '',
    },
    notification: {
      type: '',
      content: '',
    },
  };

  componentDidMount () {
    console.log(`${this.props.message} ${this.constructor.name}`);
  }

  authenticate = (username, password, history) => {
    if (username === 'geralt' && password === 'OfRivia') {
      this.setState({ authentication: { currentUser: username, isAuthenticated: true } });
      history.push('app');
      this.hideNotification();
    }
    this.showNotification('danger', Dictionary.NOTIFICATIONS.AUTHENTICATION_FAILED);
  };

  logout = () => {
    this.setState({ authentication: { isAuthenticated: false } });
    this.hideNotification();
  };

  showNotification = (type, content) => {
    this.setState({ notification: { type, content } });
  };

  hideNotification = () => {
    this.setState({ notification: { type: '', content: '' } });
  };

  render () {
    const { authentication, notification } = this.state;
    return (
      <AuthenticationContext.Provider
        value={{
          ...authentication,
          authenticate: this.authenticate,
          logout: this.logout,
        }}
      >
        <NotificationContext.Provider
          value={{
            ...notification,
            showNotification: this.showNotification,
            hideNotification: this.hideNotification,
          }}
        >
          <Router message={Dictionary.MESSAGE} />
        </NotificationContext.Provider>
      </AuthenticationContext.Provider>
    );
  }
}
