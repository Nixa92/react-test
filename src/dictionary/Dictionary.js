export default {
  MESSAGE: 'Hello from',
  HOME: 'Home',
  LOGIN: {
    TITLE: 'Authentication',
    USERNAME: 'Username',
    USERNAME_PLACEHOLDER: 'Enter username',
    PASSWORD: 'Password',
    PASSWORD_PLACEHOLDER: 'Enter password',
    BUTTON: 'Login',
  },
  ANONYMOUS: 'Anonymous',
  COMMENTS: 'Comments',
  LOGOUT: 'Logout',
  NO_POSTS: 'No available posts',
  NOTIFICATIONS: { AUTHENTICATION_FAILED: 'Authentification failed! Invalid credentials' },
  FILTER_POSTS_BY_USER: 'Find posts from user',
};
