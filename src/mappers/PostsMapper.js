import Dictionary from '../dictionary/Dictionary';

export default function mapPosts (users, posts, comments) {
  return posts.map(post => {
    const { id, userId } = post;
    let user = getUserById(users, userId);
    post.userName = getUserName(user, Dictionary.ANONYMOUS);
    post.data = getCommentsForPost(comments, id, users);
    return post;
  });
}

function getUserById (users, id) {
  return users.find(user => user.id === id);
}

function getUserByEmail (users, email) {
  return users.find(user => user.email === email);
}

function getUserName (user, defaultName) {
  return (user && user.name) || defaultName;
}

function getCommentsForPost (comments, id, users) {
  return comments.filter(comment => {
    const { postId, email } = comment;
    if (postId === id) {
      const user = getUserByEmail(users, email);
      comment.userName = getUserName(user, email);
      return comment;
    }
    return null;
  });
}
