import React from 'react';

const NotificationContext = React.createContext({
  type: '',
  content: '',
  showNotification: () => {},
  hideNotification: () => {},
});

export default NotificationContext;
